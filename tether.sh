#! /bin/bash

#Azilink tether helper
#TABLE OF CONTENTS
#	MAIN MENU (MENU00)
#	ACCESS POINT CREATOR (AP00)


#MAIN MENU (MENU00)
# Main menu that sets up Azillink, kind of works?
function func_menu {
echo "Azilink tether helper!"
echo "Main menu!"
echo "1: install tools!"
echo "2: Obtain usb device ID and prep udev rule"
echo "3: Forward ports, prep openvpn server, adjust nameserver, and reboot Network Manager!"
echo "4: Continue to access point creator!"
echo "5: DEBUG: restart ADB!"
	read n
	case $n in 
# Installs tools needed, and prompts user to install the needed apk, assuming USB debugging is enabled.		
		1) echo "Installing tools!"
			sudo pacman -S android-tools android-udev openvpn
			echo "Would you like to install Azilink.apk?(Y/N)"
				read i
				case $i in
# Creates the tether dot file to store useful shit in, ie the resolv.conf, the apk, the openvpn settings, etc.
				 Y) mkdir $HOME/.tether/
				 	cd $HOME/.tether/
					wget http://lfx.org/azilink/azilink.apk
					echo "Be sure USB debugging is enabled!"
					adb devices
					read -rsn1 -p'Press any key to continue';echo="read -rsn1 -p'Press any key to continue';echo" 
					adb install azilink.apk
					echo "Done!";;
				y) mkdir $HOME/.tether/
				 	cd $HOME/.tether/
					wget http://lfx.org/azilink/azilink.apk
					echo "Be sure USB debugging is enabled!"
					adb devices
					read -rsn1 -p'Press any key to continue';echo="read -rsn1 -p'Press any key to continue';echo" 
					adb install azilink.apk
					echo "Done!";;
				N) echo "No chosen!";;
				n) echo "No chosen!";;
				esac
				func_menu
# Lists USB devices, and asks for the ID of the phone. After being given the ID, it plugs it into the udev rule.
		2) echo "Listing usb devices!"
			echo "Please note usb ID of device!"
			lsusb
			echo "Create udev rule!"
			echo " Please input USB ID"
			read usbid
			sudo echo "SUBSYSTEM=='usb', ATTR(idVendor)=='$usbid', MODE='0666' OWNER='$USER'" > $HOME/.tether/udev.rules
				if [ -e "/etc/udev/rules.d/51-android.rules" ]; then
					sudo rm /etc/udev/rules.d/51-android.rules
					sudo cp $HOME/.tether/udev.rules /etc/udev/rules.d/51-android.rules
				else
					sudo cp $HOME/.tether/udev.rules /etc/udev/rules.d/51-android.rules
				fi			
			sudo udevadm control --reload
			echo "Done!";;
# Forwards the needed ports, checks if the .tether has been made yet, if not makes it.
		3) echo "Forwarding ports, restarting network manager, prepping openvpn, and resolving nameserver!"
			adb forward tcp:41927 tcp:41927
			echo "TCP:41927 forwarded!"
			echo "Checking for dotfiles!"
			if  [ -d "$HOME/.tether/" ]; then 
					echo "dot files found!"
				else
					mkdir $HOME/.tether/
			fi
			echo "Generating openvpn config!"
			echo "Just using the default one!"
			
#			echo -e "dev tun
# remote 127.0.0.1 41927 tcp-client
#ifconfig 192.168.56.2 192.168.56.1
# route 128.0.0.0 128.0.0.0
# socket-flags TCP_NODELAY
# keepalive 10 30
#dhcp-option DNS 192.168.56.1" > $HOME/.tether/azilink.ovpn
			echo "Stopping Network Manager!"
			sudo systemctl stop NetworkManager
			echo "Executing openvpn!"
# Jumps to the tether dot file, then executes the openvpn command with the azilink.ovpn
			cd $HOME/.tether/
			wget https://raw.githubusercontent.com/aziwoqpd/azilink/master/azilink.ovpn
			sudo openvpn azilink.ovpn > /$HOME/.tether/openvpn.log
# Creates the fixed resolv.conf, deletes the old one, then copies the updated one from the dot file.
			echo "Fixing nameserver!"
			sudo echo "nameserver 192.168.56.1" > /$HOME/.tether/resolv.conf
			sudo rm /etc/resolv.conf
			sudo cp /$HOME/.tether/resolv.conf /etc/resolv.conf
# Restarts Network Manager			
			echo "Restarting Network Manager!"
			sudo systemctl start NetworkManager
			echo "Done!";;
			
		4) echo "Proceeding to AP creator!"
			func_ap_creator;;
# Debugger to restart ADB, may be removed later			
		5) echo "Restarting ADB!"
			sudo adb kill-server
			sudo adb start-server
			sudo adb devices;;
	esac
			func_menu


}

#ACCESS POINT CREATOR! (AP00)
function func_ap_creator () {

echo "Access point creator!"
echo "Please select an option!"
echo "SSID: $var_ssid | Password: $var_password | Internet Interface: $var_wan | Wifi Interface: $var_wifi | Active : $var_active"
echo "1: Install prerequisites"
echo "2: Select SSID"
echo "3: Select password"
echo "4: Select incoming internet interface"
echo "5: Select wifi interface to use as AP"
echo "6: Start access point"
echo "7: shutdown server!"
echo "R: Return to previous menu!"

read n
case $n in
# Installs ap_creator (because I'm lazy and would rather use someone elses much better script)
	1) 	echo "Installing ap_creator!"
		sudo pacman -S ap_creator
		echo "Done!"
		func_ap_creator;;
# Requets SSID name assigns it to var_ssid
	2) echo "Please input SSID"
		read var_ssid
		echo "$var_ssid selected!"
		func_ap_creator;;
# #Prompts user for password and assigns it to var_password
	3) echo "Please select password for your access point!"
		read var_password
		echo "$var_password chosen!"
		func_ap_creator;;
# Displays interfaces( Probably gonna shrink this down somehow, or make an array of different device names, rather than
# just spewing ifconfig
	4) echo "Displaying interfaces!"
		ifconfig
		echo "Please select incoming interface!"
		read var_wan
		echo "$var_wan chosen!"
		func_ap_creator;;
# Same as above, but with wireless interface one wishes to use for the hotspot		
	5) echo "Displaying interfaces!"
		ifconfig
		echo "Please select wifi interface to use as access point!"
		read var_wifi
		echo "$var_wifi chosen!"
		func_ap_creator;;
# Executes the create_ap command based on variables.
	6) echo "Starting access point!"
		sudo create_ap $var_wifi $var_wan $var_ssid $var_password
		set $var_active == true
		func_ap_creator;;
# Kills the server.		
	7) echo "Killing Server!"
		sudo systemctl stop ap_creator
		echo "Done!"
		set $var_active == false
		func_ap_creator;;
		
	R) echo "Returning to menu!"
		func_menu;;
		
	r)echo "Returning to menu!"
		func_menu;;
	esac
}

func_menu







